(function (Drupal, once, window) {
  Drupal.behaviors.oidcRefresh = {
    attach: function (context, settings) {
      if (once('oidc-refresh', 'body', context).length) {
        var interval = settings.oidc_refresh.interval * 1000;
        var lastInteraction = Date.now();

        if (settings.oidc_refresh.interactionOnly) {
          var debouncedCallback = Drupal.debounce(
            function () {
              lastInteraction = Date.now();
            },
            1000,
            true
          );

          window.document.addEventListener('mousemove', debouncedCallback);
          window.document.addEventListener('mousedown', debouncedCallback);
          window.document.addEventListener('keydown', debouncedCallback);
          window.document.addEventListener('touchstart', debouncedCallback);
          window.document.addEventListener('scroll', debouncedCallback);
        }

        var ajaxSettings = {
          url: settings.oidc_refresh.callbackUrl
        };
        var ajaxObject = Drupal.ajax(ajaxSettings);

        window.setInterval(
          function () {
            var interactionTooLongAgo = Date.now() - lastInteraction > interval;
            if (!(settings.oidc_refresh.interactionOnly && interactionTooLongAgo)) {
              ajaxObject.execute();
            }
          },
          interval
        );
      }
    }
  };
})(Drupal, once, window);
