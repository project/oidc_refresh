<?php

namespace Drupal\oidc_refresh\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for OIDC Refresh routes.
 */
class OidcRefreshController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    // Return an empty response, this only exists to trigger the OIDC Request
    // event listener.
    return new AjaxResponse();
  }

}
