<?php

namespace Drupal\oidc_refresh\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure OIDC Refresh settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'oidc_refresh_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['oidc_refresh.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $interval = $this->config('oidc_refresh.settings')->get('interval');
    $interaction_only = $this->config('oidc_refresh.settings')->get('interaction_only');
    $form['interval'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Refresh interval'),
      '#description' => $this->t('The interval in which oidc refresh ajax calls are executed, in seconds.'),
      '#default_value' => $interval,
      '#required' => TRUE,
    ];
    $form['interaction_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only refresh on interaction'),
      '#description' => $this->t('Only trigger the refresh if the user has interacted with the page since the last refresh or page load.'),
      '#default_value' => $interaction_only,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('oidc_refresh.settings')
      ->set('interval', (int) $form_state->getValue('interval'))
      ->set('interaction_only', (bool) $form_state->getValue('interaction_only'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
